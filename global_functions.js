const writeln = function (text) {
  document.writeln(text);
};

const write = function (text) {
  document.write(text);
};

const writeChapter = function(chapterName) {
  write('**********');
  write(`<h2>${chapterName}</h2>`);
}

const writeSubChapter = function(subChapterName) {
  write('**********');
  write(`<h3>${subChapterName}</h3>`);
}

const writeDevisiorLine = function() {
  writeln('---------------------------------');
}

const writeArrayItems = function (array) {
  writeln('Items in array:');
  array.forEach((item, index) => {
    writeln(index + ': ' + item);
  });
};

Object.prototype.addMethod = function (functionName, functionToAdd) {
  if (!this.prototype[functionName]) {
    this.prototype[functionName] = functionToAdd;
    writeln(`Method called:'${functionName}' has been add to Function.protorype`);
    return this;
  } else {
    writeln(`Method called:'${functionName}' add already existing.`);
  }
};

const scrollDown = function (){
  window.scrollTo({ left: 0, top: document.body.scrollHeight, behavior: "smooth" });
};