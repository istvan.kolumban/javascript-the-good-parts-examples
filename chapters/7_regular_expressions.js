// Created: 12/07/2021

writeChapter('VII. REGULAR EXPRESSIONS');
writeSubChapter('An Example:');
writeln('Regular expression that matches URLs');
const parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;

const writeUrlParts = function (resultOfExec) {
  const names = ['url', 'scheme', 'slash', 'host', 'port', 'path', 'query', 'hash'];
  const blanks = '       ';
  for (let i = 0; i < names.length; i++) {
    writeln(`${names[i]}:${blanks.substring(names[i].length)}${resultOfExec[i]}`);
  }
  writeln('');
};

const url = 'http://www.ora.com:80/goodparts?q#fragment';
writeUrlParts(parse_url.exec(url));

const url2 = 'http://127.0.0.1:5501/index.html';
writeUrlParts(parse_url.exec(url2));

const url3 = 'https://www.youtube.com/watch?v=EPXz7700lfY&ab_channel=BlackMixTape';
writeUrlParts(parse_url.exec(url3));

const url4 = 'angular.io/guide/forms-overview';
writeUrlParts(parse_url.exec(url4));

const url5 = 'https://www.google.com/some/resource?query=help';
writeUrlParts(parse_url.exec(url5));

scrollDown();
