// Created: 11/24/2021

writeChapter('VI. ARRAYS');
writeSubChapter('Array Literals:');

const arrayOfEverithing = ['text', 98.6, Math.PI, true, null, undefined, NaN, Infinity, ['Other array', 'I am nested'], { key: 'value' }];

writeln(arrayOfEverithing.length);

writeArrayItems(arrayOfEverithing);

writeSubChapter('Length:');
const arrayWithMissingElements = [0, 1];
arrayWithMissingElements['10'] = 10;
writeln(arrayWithMissingElements.length);

writeln('If I set the length of the array less than the actual length, the rest of the items will be deleted');
const arrayWithSetLength = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
writeln('Length of the array:' + arrayWithSetLength.length);
arrayWithSetLength.length = 2;
writeln('Length of the array was reset to 2: ' + arrayWithSetLength.length);
writeArrayItems(arrayWithSetLength);
writeln('Third item in the array: ' + arrayWithSetLength[2]);
arrayWithSetLength.push(11);
writeArrayItems(arrayWithSetLength);
writeln('Length of the array:' + arrayWithSetLength.length);

writeSubChapter('Delete:');
const arrayToDeleteOneItem = [100, 200, 300];
writeArrayItems(arrayToDeleteOneItem);
delete arrayToDeleteOneItem[1];
writeln('Delete second item. Indexes will not be updated');
writeArrayItems(arrayToDeleteOneItem);
let arrayToSlice = [100, 200, 300, 400, 500];
writeArrayItems(arrayToSlice);
arrayToSlice.splice(1, 1);
writeln('Splice from second item. Indexes will be updated');
writeArrayItems(arrayToSlice);

writeSubChapter('Enumeration:');
writeln("Ude conventional 'for' loop instead of 'for in', because for in does not process the values in order.");

writeSubChapter('Confusion: when to use array and when to use object?');
const simpleArray = [1, 2, 3];
writeln('Unfortunately typeof an array is object. Example: ' + typeof simpleArray);
Array.prototype.isArray = function () {
  return typeof this === 'object' && this.constructor === Array;
};

writeln('isArray function is able to decide the type. Example: ' + simpleArray.isArray());

writeSubChapter('Methods:');

Array.addMethod('reduce', function (f, value) {
  for (let i = 0; i < this.length; i++) {
    value = f(this[i], value);
  }
  return value;
});

const add = function (number1, number2) {
  return number1 + number2;
};
const multiply = function (number1, number2) {
  return number1 * number2;
};
const arrayOnCallReduce = [1, 2, 3, 4, 5];
writeln(arrayOnCallReduce.reduce(add, 0));
writeln(arrayOnCallReduce.reduce(multiply, 1));

writeSubChapter('Dimensions:');

Array.dim = function(dimension, initial){
  const a = [];
  for (let i = 0; i < dimension; i++) {
    a[i] = initial;
  }
  return a;
}
writeln("make an array with containing 10 zeroes.");
const arrayOfZeroes = Array.dim(10, 0);
writeArrayItems(arrayOfZeroes);

scrollDown();