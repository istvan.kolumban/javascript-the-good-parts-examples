// Created: 10/26/2021

writeln('hello world');
// Define new methods
Function.prototype.method = function (name, func) {
  this.prototype[name] = func;
  return this;
};
writeln('**********');
write('<h2>II. GRAMMAR</h2>');
// Numbers
write('<h3>Numbers:</h3>');
const numInt = 1;
const numDouble = 1.0;
writeln('There is no difference between 1 and 1.0 numbers.');
write('numInt === numDouble: ');
writeln(numInt === numDouble);
writeln('----------');

const numNaN = NaN;
writeln('NaN is not equal to any value, including itself.');
write('numNaN === NaN: ');
writeln(numNaN === NaN);
writeln('isNaN(numNaN): ' + isNaN(numNaN));
writeln(Infinity);
writeln('----------');
write('<h3>Statements:</h3>');
const obj = {
  size: 10,
  color: 'blue',
};
for (myvar in obj) {
  if (obj.hasOwnProperty(myvar)) {
    writeln('Own property: ' + myvar);
  }
}