// Created: 10/27/2021

writeln('**********');
write('<h2>III. OBJECTS</h2>');
// Inherit properties form another object
let empty_object = {};
let dad = {
  'first-name': 'Istvan',
  'last-name': 'Kolumban',
};
write('<h3>Retrieval:</h3>');
let middleName = dad['middle-name'] || '(none)';
writeln('Give default value if the retreived value is undefined: ' + middleName);
let lastName = dad['last-name'] || 'Fodor';
writeln('Do not give default value if the retreived value is defined: ' + lastName);

dad.middleName = 'Cs.';
writeln(dad.middleName);
writeln('----------');
write('<h3>Prototype:</h3>');

// if the Object.create is undefined
if (typeof Object.create !== 'function') {
  // define the create function
  Object.create = (object) => {
    // create a new function
    let F = () => {};
    // set it's propertie to the given object
    F.prototype = object;
    // return the function
    return new F();
  };
}

let daughter = Object.create(dad);
writeln('The daughter object should inherit every properties form dad object.');
writeln(daughter['first-name']);
writeln(daughter['last-name']);
writeln(daughter.middleName);
daughter['first-name'] = 'Zsofi';
writeln(daughter['first-name']);
daughter.gender = 'female';

writeln('----------');
write('<h3>Enumeration:</h3>');

let getProperties = (object) => {
  for (let propertyName in object) {
    if (typeof propertyName !== 'function') {
      writeln('Property: ' + propertyName);
    } else {
      writeln('Not Property: ' + propertyName);
    }
  }
};

getProperties(daughter);

writeln('----------');
write('<h3>Delete:</h3>');

writeln("Before delete: daughter['first-name']=" + daughter['first-name']);
delete daughter['first-name'];
writeln("After delete: daughter['first-name']=" + daughter['first-name']);
delete dad['first-name'];
writeln("After delete form it's prototype: daughter['first-name']=" + daughter['first-name']);
dad['first-name'] = 'Istvan';
daughter['first-name'] = 'Zsofi';

writeln('----------');
write('<h3>Global Abatement:</h3>');

let MYAPP = {};
MYAPP.dad = dad;
MYAPP.daughter = daughter;
writeln('Create a container global variable for everything that is global. MYAPP.daughter.gender=' + MYAPP.daughter.gender);
