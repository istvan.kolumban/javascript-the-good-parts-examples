// Created: 11/18/2021

write('**********');
write('<h2>V. INHERITANCE</h2>');

write('<h3>Pseudoclassical: (Not recomended option)</h3>');

Function.prototype.addMethod = function (functionName, functionToAdd) {
  if (!this.prototype[functionName]) {
    this.prototype[functionName] = functionToAdd;
    writeln(`Method called:'${functionName}' has been add to Function.protorype`);
    return this;
  } else {
    writeln(`Method called:'${functionName}' to add already existing.`);
  }
};

// Define a constructor and augment its prototype:
const Mammal = function (name) {
  this.name = name;
  this.pregnantTime = 10;
};

Mammal.prototype.get_name = function () {
  return this.name;
};

Mammal.prototype.says = function () {
  return this.saying || '';
};

const mammal = new Mammal('Istvan');
writeln(mammal.get_name());
writeln(mammal.says());

const Cat = function (name) {
  this.name = name;
  this.saying = 'meow';
};

Cat.prototype = new Mammal();

Cat.prototype.purr = function (n) {
  let sound = '';
  for (let i = 0; i < n; i++) {
    if (sound) {
      sound += '-';
    }
    sound += 'r';
  }
  return sound;
};
Cat.prototype.get_name = function () {
  return this.says() + ' ' + this.name + ' ' + this.says();
};

const myCat = new Cat('Cirmos');
writeln(myCat.says());
writeln(myCat.purr(10));
writeln(myCat.get_name());

writeln('Better approach to inherit: create and add inherits funtcion');
Function.addMethod('inherits', function (Parent) {
  this.prototype = new Parent();
  return this;
});

const Dog = function (name) {
  this.name = name;
  this.saying = 'wauuu';
};

Dog.inherits(Mammal);

const myDog = new Dog('Tappancs');
writeln(myDog.pregnantTime);

write('**********');
write('<h3>Object Specifiers:</h3>');
writeln('How to call a function if there are a lot of paramteters?');

// I don't understand how to use this???!!!
// const myObject = maker({
//   first: f,
//   last: l,
//   middle: m,
//   state: s,
//   city: c,
// });
write('**********');
write('<h3>Prototypal: solve the inheritance with different approach</h3>');

const myMammal = {
  name: 'Herb the Mammal',
  get_name: function () {
    return this.name;
  },
  says: function () {
    return this.saying || '';
  },
};

if (typeof Object.create !== 'function') {
  Object.create = (object) => {
    let F = () => {};
    F.prototype = object;
    return new F();
  };
}

const myMouse = Object.create(myMammal);
myMouse.name = 'Stuard';
myMouse.saying = 'cin cin';
myMouse.get_name = function () {
  return this.says() + ' ' + this.name + ' ' + this.says();
};
writeln(myMouse.get_name());

write('**********');
write('<h3>Functional: best approach so far. Achieve private attributes and methods</h3>');

Object.addMethod('superior', function (name) {
  const that = this;
  const method = that[name];
  return function () {
    return method.apply(that, arguments);
  };
});

const mammal_functional = function (spec) {
  var that = {};
  that.get_name = function () {
    return spec.name;
  };
  that.says = function () {
    return spec.saying || '';
  };
  return that;
};

const myMammal_functional = mammal_functional({ name: 'Herb' });

const cat_functional = function (spec) {
  spec.saying = spec.saying || 'meow';
  const that = mammal_functional(spec);

  that.super_get_name = that.superior('get_name');

  that.purr = function (n) {
    let s = '';
    for (let i = 0; i < n; i++) {
      s += s ? '-r' : 'r';
    }
    return s;
  };

  that.get_name = function () {
    return that.says() + ' ' + that.super_get_name() + ' ' + that.says();
  };

  return that;
};

const myCat_functional = cat_functional({ name: 'Henrietta' });
writeln(myCat_functional.purr(10));
writeln(myCat_functional.get_name());

write('**********');
write('<h3>Parts:</h3>');
// We can compose objects out of sets of parts. I have no idea what this section is about.
