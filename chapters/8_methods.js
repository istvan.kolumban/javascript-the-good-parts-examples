// Created: 01/03/2022
writeChapter('VIII. Methods');
writeSubChapter('Array:');
writeln('Array before shift:');
const arrayToShift = ['a', 'b', 'c'];
writeArrayItems(arrayToShift);
arrayToShift.shift();
writeln('Array after shift:');
writeArrayItems(arrayToShift);
writeDevisiorLine();

writeln('Array before unshift:');
const arrayToUnshift = ['a', 'b', 'c'];
writeArrayItems(arrayToUnshift);
arrayToUnshift.unshift('1', '2', '3');
writeln('Array after unshift:');
writeArrayItems(arrayToUnshift);
writeDevisiorLine();

writeln('Array before slice:');
const arrayToSlice = ['a', 'b', 'c', 'd', 'e'];
writeArrayItems(arrayToSlice);
writeln('slice(0,1)');
writeArrayItems(arrayToSlice.slice(0, 1));
writeln('slice(1,3)');
writeArrayItems(arrayToSlice.slice(1, 3));
writeln('slice(3)');
writeArrayItems(arrayToSlice.slice(3));
writeDevisiorLine();

writeln('Array to sort:');
const arrayToSort = [4, 13, 5, 3, 99, 102, 11];
writeArrayItems(arrayToSort);
writeln('Wrong sort: .sort()');
writeArrayItems(arrayToSort.sort());
writeln('Good sort: .sort(goodCompareFunction)');
writeArrayItems(
  arrayToSort.sort(function (a, b) {
    return a - b;
  })
);
writeDevisiorLine();

writeln('Array before splice:');
const arrayToSplice = ['a', 'b', 'c', 'd', 'e'];
writeArrayItems(arrayToSplice);
arrayToSplice.splice(1, 2, 'newItem1', 'newItem2', 'newItem3');
writeArrayItems(arrayToSplice);
writeSubChapter('String:');
const textInSearch = 'and in it he says "Any damn fool could';
const pos = textInSearch.search(/"/);
writeln(pos);
scrollDown();