// Created: 10/28/2021

write('**********');
write('<h2>IV. FUNCTIONS</h2>');
write('<h3>Function Literal:</h3>');
let add = function (number1, number2) {
  return number1 + number2;
};

writeln('typeof add: ' + typeof add);
writeln('typeof add(): ' + typeof add());

let calculator = {
  add,
  PI: Math.PI,
  result: 0,
};
writeln(calculator.add(4, 5));
writeln(calculator.add(3, calculator.PI));

if (typeof Object.create === 'undefined') {
  Object.create = (parent) => {
    let F = () => {};
    F.prototype = parent;
    return new F();
  };
}

let calculatorPlus = Object.create(calculator);
writeln(calculatorPlus.PI * calculatorPlus.add(3, 1));
calculatorPlus.multiply = (number1, number2) => {
  return number1 * number2;
};
calculatorPlus.value = 10;

for (let item in calculatorPlus) {
  write(item + ': ');
  if (calculatorPlus.hasOwnProperty(item)) {
    write('own property;');
  } else {
    write('not own property;');
  }
  writeln();
}

write('----------');
write('<h3>Invocation:</h3>');
write('<h4>The Method Invocation Pattern:</h4>');
writeln('There is a big difference between function(){} and ()=>{}');
// function() regognize the this parameter as it's object
calculatorPlus.piSquare = function () {
  return this.PI * this.PI;
};
writeln('Method invocation: ' + calculatorPlus.piSquare());
// The lambda function does not recognize the this parameter as it's object.
calculatorPlus.piDouble = () => {
  return 2 * this.PI;
};
writeln("Lambda function's this points to window object: " + calculatorPlus.piDouble());

write('----------');
write('<h4>The Function Invocation Pattern:</h4>');

function times2(value) {
  return 2 * value;
}
// Use a function to create a method for an object
calculatorPlus.times2 = function () {
  let that = this;
  function helper() {
    return times2(that.value);
  }
  return helper();
};
writeln('Use a function to create a method for an object');
writeln(calculatorPlus.times2());

write('----------');
write('<h4>The Constructor Invocation Pattern: (Not recomended)</h4>');

// Create a constructor function for Person
let Person = function (name) {
  this.name = name;
};

// Give all instances of Person a public method
Person.prototype.getName = function () {
  return this.name;
};

// Create a new instance of Person
let mother = new Person('Rita');
let grandMother = new Person('Ani');
writeln(mother.getName());
writeln(grandMother.getName());

write('----------');
write('<h4>The Apply Invocation Pattern:</h4>');

let pow = function (number1, number2) {
  this.result = Math.pow(number1, number2);
};
let superCalculator = Object.create(calculator);
// apply unknown apply method for superCalculator
pow.apply(superCalculator, [2, 10]);
writeln(superCalculator.result);

superCalculator.name = 'Super Calsulator';
// apply getName method for superCalculator
writeln(Person.prototype.getName.apply(superCalculator));

write('----------');
write('<h3>Arguments:</h3>');
writeln('Not recommended to use the argument variable as an array:');
let sum = function () {
  let sum = 0;
  for (let index = 0; index < arguments.length; index++) {
    sum += arguments[index];
  }
  return sum;
};

writeln(sum(1, 2));
writeln(sum(1, 2, 3, 4, 5));

write('----------');
write('<h3>Exceptions:</h3>');

// Extend add function by type check
add = function (number1, number2) {
  if (typeof number1 !== 'number' || typeof number2 !== 'number') {
    throw {
      name: 'TypeError',
      message: 'arguments has to be numbers',
    };
  }
  return number1 + number2;
};

let try_add = function (varibale1, variable2) {
  let result = 0;
  try {
    result = add(varibale1, variable2);
    writeln(`${varibale1} + ${variable2} = ${result}`);
  } catch (e) {
    writeln(e.name + ': ' + e.message);
  }
};

try_add(4, 'text');
try_add(true, 'text');
try_add(Person, 5);
try_add(calculatorPlus.PI, calculatorPlus.PI);

write('----------');
write('<h3>Augmenting Types:</h3>');
// By this approach we are able to add desired function to the Function prototype
Function.prototype.addMethod = function (name, functionToAdd) {
  if (!this.prototype[name]) {
    this.prototype[name] = functionToAdd;
    return this;
  } else {
    writeln(`Method called:'${name}' to add already existing.`);
  }
};

Number.addMethod('isEven', function () {
  return this % 2 === 0;
});

Number.addMethod('toInteger', function () {
  if (this < 0) {
    return Math.ceil(this);
  } else {
    return Math.floor(this);
  }
});
Number.addMethod('toInteger', function () {
  return 0;
});

writeln((5).isEven());
writeln(Math.PI.isEven());
writeln(Math.PI.toInteger());
writeln((-10 / 3).toInteger());

write('----------');
write('<h3>Recursion:</h3>');

writeln('Walk the DOM');

const walk_the_DOM = function walk(node, func) {
  // Execute given function
  func(node);
  // Walk through all children
  node = node.firstChild;
  while (node) {
    walk(node, func);
    node = node.nextSibling;
  }
};

const getElementsByAttribute = function (att, value) {
  let results = [];
  walk_the_DOM(document.body, function (node) {
    const actual = node.nodeType === 1 && node.getAttribute(att);
    if (typeof actual === 'string' && (actual === value || typeof value !== 'string')) {
      results.push(node);
    }
  });
  return results;
};

const getElementsByNodeName = function (nodeName) {
  let results = [];
  walk_the_DOM(document.body, function (node) {
    const actualNodeName = node.nodeType === 1 && node.nodeName;
    if (typeof actualNodeName === 'string' && actualNodeName.toLocaleLowerCase() === nodeName) {
      results.push(node);
    }
  });
  return results;
};

const getElementById = function (id) {
  let result = [];
  walk_the_DOM(document.body, function (node) {
    const actualId = node.nodeType === 1 && node.getAttribute('id');
    if (typeof actualId === 'string' && actualId === id) {
      result.push(node);
    }
  });
  return result[0];
};

const results = getElementsByNodeName('h3');
writeln('List of all h3 html element: ');
for (let index = 0; index < results.length; index++) {
  writeln(results[index].innerHTML);
}

writeln('<div id="bestDivEver">Try to find this div</div>');
const foundItemById = getElementById('bestDivEver');
writeln('Use getElementById to find the element with bestDivEver id: ' + foundItemById.innerHTML);

// tail recursion but JavaScript does not optimize it.
const factorial = function (number) {
  return number > 1 ? number * factorial(number - 1) : 1;
};

writeln(factorial(4));
writeln(factorial(3));

write('----------');
write('<h3>Closure:</h3>');

const myObjectPlus = (function () {
  let value = 0;
  return {
    increment: function (inc) {
      value += typeof inc === 'number' ? inc : 1;
    },
    getValue: function () {
      return value;
    },
  };
})();

writeln(myObjectPlus.getValue());
myObjectPlus.increment(15);
writeln(myObjectPlus.getValue());
writeln("The function's scope keeps the variable hidden from the resto of the program: " + myObjectPlus.value);

const quo = (function () {
  let status = '';
  return {
    get_status: function () {
      writeln('Status: ' + status);
      return status;
    },
    set_status: function (newStatus) {
      if (typeof newStatus === 'string') {
        status = newStatus;
      }
    },
  };
})();

quo.set_status('cool');
quo.get_status();
quo.set_status('awesome');
quo.get_status();

var rgbToHex = function (rgb) {
  var hex = Number(rgb).toString(16);
  if (hex.length < 2) {
    hex = '0' + hex;
  }
  return hex;
};

const fade = function (node) {
  let red = 0;
  let green = 0;
  let blue = 0;
  let numberOfCalls = 0;
  var step = function () {
    numberOfCalls++;
    let redHex = rgbToHex(red);
    let greenHex = rgbToHex(green);
    let blueHex = rgbToHex(blue);
    node.style.backgroundColor = '#' + redHex + greenHex + blueHex;
    if (red < 255) {
      red++;
      setTimeout(step, 1);
    } else if (green < 255) {
      // console.log(`red: ${red}, green: ${green}, blue: ${blue}`);
      green++;
      red = 0;
      setTimeout(step, 1);
    } else if (blue < 255) {
      // console.log(`red: ${red}, green: ${green}, blue: ${blue}`);
      blue++;
      red = 0;
      green = 0;
      setTimeout(step, 1);
    } else {
      red = 0;
      green = 0;
      blue = 0;
      setTimeout(step, 1);
      numberOfCalls = 0;
    }
  };
  step();
};

// create a div element
writeln('The following div will has all of the possible colors. There are 16,777,216 possibility, so it takes time to wait for it :P');
const divToFadeId = 'divToFadeId';
writeln(`<div id="${divToFadeId}"<div>`);
let divToFade = getElementById(divToFadeId);
divToFade.style.height = '100px';
divToFade.style.width = '100px';
fade(divToFade);

writeln();
writeln();
writeln();
writeln();
writeln();
writeln('----------');
write('<h3>Module:</h3>');
String.addMethod(
  'deentityfy',
  (function () {
    const entity = {
      quot: '"',
      lt: '<',
      gt: '>',
    };
    return function () {
      return this.replace(/&([^&;]+);/g, function (a, b) {
        const r = entity[b];
        return typeof r === 'string' ? r : a;
      });
    };
  })()
);
writeln('&lt;div class=&quot;divClass&quot;&gt;The huge content. &lt;/div&gt;'.deentityfy());
writeln('----------');

// What is the difference??? I will use different approaches to create objects with similar behaviors//
// This function will return an object which will contain functions but WON'T contain the private variable which is awesome.
const whenFunctionReturnFunctions = function () {
  let innerCounter = 0;
  return {
    increaseInnerVariable: function () {
      innerCounter++;
      writeln(`Inner counter incresed: ${innerCounter}`);
    },
    resetInnerCounter: function () {
      innerCounter = 0;
      writeln(`Inner counter was reset to: ${innerCounter}`);
    },
  };
};
// I am not able to reach the private variable
const instanceOfWhenFunctionReturnFunctions = whenFunctionReturnFunctions();
instanceOfWhenFunctionReturnFunctions.increaseInnerVariable();
instanceOfWhenFunctionReturnFunctions.increaseInnerVariable();
instanceOfWhenFunctionReturnFunctions.resetInnerCounter();
instanceOfWhenFunctionReturnFunctions.increaseInnerVariable();
writeln('----------');
// With this approach I will create a function and call it immediately.
// In the previous example ${whenFunctionReturnFunctions} I needed to call it to create an object:
// const instanceOfWhenFunctionReturnFunctions = whenFunctionReturnFunctions();
// But now I won't need to do it. I can use the ${objectFromFunctionDefinitionAndInstantCall} variable since it is already an object.
const objectFromFunctionDefinitionAndInstantCall = (function () {
  let innerCounter = 0;
  return {
    increaseInnerVariable: function () {
      innerCounter++;
      writeln(`Inner counter incresed: ${innerCounter}`);
    },
    resetInnerCounter: function () {
      innerCounter = 0;
      writeln(`Inner counter was reset to: ${innerCounter}`);
    },
  };
})();

objectFromFunctionDefinitionAndInstantCall.increaseInnerVariable();
objectFromFunctionDefinitionAndInstantCall.increaseInnerVariable();
objectFromFunctionDefinitionAndInstantCall.increaseInnerVariable();

writeln('----------');
// In this object has a variable which unfortunatelly is public so it free for change, or even delete
const whenObjectHasFunctions = {
  innerCounter: 0,
  increaseInnerVariable: function () {
    this.innerCounter++;
    writeln(`Inner counter incresed: ${this.innerCounter}`);
  },
  resetInnerCounter: function () {
    this.innerCounter = 0;
    writeln(`Inner counter was reset to: ${this.innerCounter}`);
  },
};
// I am able to access to variable
whenObjectHasFunctions.increaseInnerVariable();
whenObjectHasFunctions.increaseInnerVariable();
whenObjectHasFunctions.innerCounter = 10;
whenObjectHasFunctions.increaseInnerVariable();
delete whenObjectHasFunctions.innerCounter;
whenObjectHasFunctions.increaseInnerVariable();

writeln('----------');
write('<h3>Cascade:</h3>');

const myBoxDivId = 'myBoxDivId';
writeln(`&lt;div id=&quot;${myBoxDivId}&quot;&gt;This box div will be modified by cascading. &lt;/div&gt;`.deentityfy());
const myBoxDiv = getElementById(myBoxDivId);

writeln('----------');
write('<h3>Memoization:</h3>');
const fibonacci = (function () {
  let memo = [0, 1];
  let fib = function(n){
    let result = memo[n];
    if (typeof result !== 'number'){
      result = fib(n-1) + fib(n-2);
      memo[n]=result;
    }
    return result;
  }
  return fib;
}());

for(let i=0;i<=10;i++){
  writeln(`${i}: ${fibonacci(i)}`);
}
